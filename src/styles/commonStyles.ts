import styled from 'styled-components'

export const colors = {
  text: '#1D1F22',
  primary: '#5ECE7B',
  white: '#FFFFFF',
  gary: '#E5E5E5',
  backgroundArrowCartGallery: 'rgba(0,0,0,0.73)',
  backgroundActiveOverlayCart: 'rgba(57,55,72,0.22)',
  onHoverCurrency: '#EEEEEE',
  hoverCard: 'rgba(168, 172, 176, 0.19)',
}

export const fonts = {
  Raleway: "'Raleway', sans-serif",
  Roboto: "'Roboto', sans-serif",
  RobotoCondensed: "'Roboto Condensed', sans-serif",
  SourceSansPro: "'Source Sans Pro', sans-serif",
}

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  margin: 0;
  cursor: default;
`

export const Container = styled.div`
  max-width: 1250px;
  width: 100%;
  margin: 0 auto;
  padding: 0 20px;
`

export const Arrow = styled.div`
  border: solid ${colors.text};
  border-width: 0 1px 1px 0;
  display: inline-block;
  padding: 1px;
  height: 3px;
  width: 3px;
`
