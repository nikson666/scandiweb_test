export const routes = {
  categoryRoute: 'category',
  productRoute: 'product',
  cartRoute: 'cart',
}
