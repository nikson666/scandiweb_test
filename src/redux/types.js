export const categoriesTypes = {
  SET_ACTIVE_CATEGORY: 'SET_ACTIVE_CATEGORY',
  UNMOUNT_COMPONENT: 'UNMOUNT_COMPONENT',
}

export const cartTypes = {
  SET_PRODUCTS_OF_CART: 'SET_PRODUCTS_OF_CART',
  SET_CHANGE_COUNTER: 'SET_CHANGE_COUNTER',
  SET_IS_CART_OVERLAY: 'SET_IS_CART_OVERLAY',
}

export const productTypes = {
  SET_PRODUCT_ID: 'SET_PRODUCT_ID',
  SET_INDEX_OF_IMAGE: 'SET_INDEX_OF_IMAGE',
  SET_SELECTED_ATTRIBUTES: 'SET_SELECTED_ATTRIBUTES',
  RESET_SELECTED_ATTRIBUTES: 'RESET_SELECTED_ATTRIBUTES',
}

export const currenciesTypes = {
  SET_CURRENCY_SYMBOL: 'SET_CURRENCY_SYMBOL',
  SET_ACTIVE_SELECTOR_FLAG: 'SET_ACTIVE_SELECTOR_FLAG',
}
