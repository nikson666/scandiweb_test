import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'
import { cartReducer } from './reducers/cartReducer'
import { categoriesReducer } from './reducers/categoriesReducer'
import { currenciesReducer } from './reducers/currenciesReducer'
import { productReducer } from './reducers/productReducer'

export const store = configureStore({
  reducer: {
    categories: categoriesReducer,
    currencies: currenciesReducer,
    product: productReducer,
    cart: cartReducer,
  },
})

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
