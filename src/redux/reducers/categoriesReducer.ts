import { categoriesTypes } from '../types'

interface Action {
  type: string;
  payload?: any;
}

interface State {
  activeCategory: string;
}

const initialState: State = {
  activeCategory: '',
}

export const categoriesReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case categoriesTypes.SET_ACTIVE_CATEGORY:
      return { ...state, activeCategory: action.payload }
    default:
      return state
  }
}

export const setActiveCategoryAC = (category: string): Action => ({
  type: categoriesTypes.SET_ACTIVE_CATEGORY,
  payload: category,
})
