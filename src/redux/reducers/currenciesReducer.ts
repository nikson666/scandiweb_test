import { currenciesTypes } from '../types'

interface Action {
  type: string;
  payload?: any;
}

interface State {
  currencySymbol: string;
  activeSelectorFlag: boolean;
}

const initialState: State = {
  currencySymbol: '$',
  activeSelectorFlag: false,
}

export const currenciesReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case currenciesTypes.SET_CURRENCY_SYMBOL:
      return { ...state, currencySymbol: action.payload }
    case currenciesTypes.SET_ACTIVE_SELECTOR_FLAG:
      return { ...state, activeSelectorFlag: action.payload }
    default:
      return state
  }
}

export const setCurrencySymbolAC = (currencySymbol: string): Action => ({
  type: currenciesTypes.SET_CURRENCY_SYMBOL,
  payload: currencySymbol,
})

export const setActiveSelectorFlagAC = (flag: boolean): Action => ({
  type: currenciesTypes.SET_ACTIVE_SELECTOR_FLAG,
  payload: flag,
})
