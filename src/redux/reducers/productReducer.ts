import { productTypes } from '../types'

interface Action {
  type: string;
  payload?: any;
}

interface State {
  productId: string;
  indexOfImage: number;
  selectedAttributes: Record<string, string>;
}

const initialState: State = {
  productId: '',
  indexOfImage: 0,
  selectedAttributes: {
    // [key: attribute id ]: item id
  },
}

export const productReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case productTypes.SET_PRODUCT_ID:
      return { ...state, productId: action.payload }
    case productTypes.SET_INDEX_OF_IMAGE:
      return { ...state, indexOfImage: action.payload }
    case productTypes.SET_SELECTED_ATTRIBUTES:
      return {
        ...state,
        selectedAttributes: {
          ...state.selectedAttributes,
          [action.payload.attributeId]: action.payload.itemId,
        },
      }
    case productTypes.RESET_SELECTED_ATTRIBUTES:
      return {
        ...state,
        selectedAttributes: action.payload,
      }
    default:
      return state
  }
}

export const setProducIdAC = (productId: string): Action => ({
  type: productTypes.SET_PRODUCT_ID,
  payload: productId,
})

export const setIndexOfImageAC = (index: number): Action => ({
  type: productTypes.SET_INDEX_OF_IMAGE,
  payload: index,
})

export const setSelectedAttributesAC = (
  attributeId: string,
  itemId: string,
): Action => ({
  type: productTypes.SET_SELECTED_ATTRIBUTES,
  payload: {
    attributeId,
    itemId,
  },
})

export const resetSelectedAttributesAC = (
  selectedAttributes: object,
): Action => ({
  type: productTypes.RESET_SELECTED_ATTRIBUTES,
  payload: selectedAttributes,
})
