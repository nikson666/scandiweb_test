import { cartTypes } from '../types'

interface Action {
  type: string;
  payload?: any;
}

interface ProductOfCartPrice {
  readonly amount: number;
  readonly currency: {
    label: string;
    symbol: string;
  };
}

interface ProductOfCart {
  productId: string;
  selectedAttributes: Record<string, string>;
  uniqKey: string;
  counter: number;
  prices: ProductOfCartPrice[];
}

interface State {
  productsOfCart: ProductOfCart[];
  isCartOverlay: boolean;
}

const initialState: State = {
  productsOfCart: [],
  isCartOverlay: false,
}

export const cartReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case cartTypes.SET_PRODUCTS_OF_CART:
      return { ...state, productsOfCart: action.payload }
    case cartTypes.SET_CHANGE_COUNTER:
      return {
        ...state,
        productsOfCart: action.payload,
      }
    case cartTypes.SET_IS_CART_OVERLAY:
      return {
        ...state,
        isCartOverlay: action.payload,
      }
    default:
      return state
  }
}

export const setProductsOfCartAC = (
  productsOfCart: ProductOfCart[],
): Action => ({
  type: cartTypes.SET_PRODUCTS_OF_CART,
  payload: productsOfCart,
})

export const setChangeCounterAC = (
  productsOfCart: ProductOfCart[],
): Action => ({
  type: cartTypes.SET_CHANGE_COUNTER,
  payload: productsOfCart,
})

export const setIsCartOverlayAC = (isActive: boolean): Action => ({
  type: cartTypes.SET_IS_CART_OVERLAY,
  payload: isActive,
})
