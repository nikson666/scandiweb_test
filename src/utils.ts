interface Price {
  readonly amount: number;
  readonly currency: {
    label: string;
    symbol: string;
  };
}

interface ProductOfCart {
  productId: string;
  selectedAttributes: Record<string, string>;
  uniqKey: string;
  counter: number;
  prices: Price[];
}

export const getIndexOfPrice = (prices: Array<Price>, symbol: string) => {
  const indexOfPrice: number = prices.findIndex(
    (item) => item.currency.symbol === symbol,
  )
  return indexOfPrice
}

export const getProductsOfCartFromLocalStorage = async () => {
  try {
    const productsOfCartFromLS = await localStorage.getItem('ScandiawebCart')

    if (productsOfCartFromLS) {
      const parseProductsOfCart: Promise<ProductOfCart[]> = await JSON.parse(
        productsOfCartFromLS,
      )

      return parseProductsOfCart
    } return []
  } catch (error) {
    console.log('getProductsOfCartFromLocalStorage ERROR:', error)
  }
}

export const setProductsOfCartOnLocalStorage = async (
  newProductsOfCart: ProductOfCart[],
) => {
  try {
    localStorage.setItem('ScandiawebCart', JSON.stringify(newProductsOfCart))
  } catch (error) {
    console.log('setProductsOfCartFromLocalStorage ERROR:', error)
  }
}

export const getCounter = (productsOfCart: ProductOfCart[]) => {
  let counter = 0

  const arrAllCounterOfProducts: number[] = productsOfCart?.map((product) => product.counter)

  if (arrAllCounterOfProducts.length) {
    counter = arrAllCounterOfProducts.reduce(
      (accumulator, value) => accumulator + value,
    )
  }

  return counter
}

export const getTotalPrice = (
  productsOfCart: ProductOfCart[],
  symbol: string,
) => {
  try {
    if (productsOfCart.length) {
      const indexOfPrice = getIndexOfPrice(productsOfCart[0]?.prices, symbol)

      const arrayOfTotalAmountOf: number[] = productsOfCart.map(
        (item) => item.prices[indexOfPrice].amount * item.counter,
      )

      const totalAmountOf = arrayOfTotalAmountOf?.reduce(
        (accumulator, value) => accumulator + value,
      )

      return totalAmountOf.toFixed(2)
    } return 0
  } catch (error) {
    console.log('ERROR', error)
  }
}

export const getItemsFromSessionStorage = async (key: string) => {
  try {
    const itemsFromSS = await sessionStorage.getItem(key)
    if (itemsFromSS) {
      const parseItemsFromSS: string = await JSON.parse(
        itemsFromSS,
      )
      return parseItemsFromSS
    } return ''
  } catch (error) {
    console.log('ERROR', error)
  }
}

export const setItemToSessionStorage = (key: string, setItem: any) => {
  try {
    const setItemStringify = JSON.stringify(setItem)
    sessionStorage.setItem(key, setItemStringify)
  } catch (error) {
    console.log('ERROR', error)
  }
}
