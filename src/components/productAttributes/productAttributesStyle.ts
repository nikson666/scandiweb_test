import styled from 'styled-components'
import { fonts, colors } from '../../styles/commonStyles'

interface AttributesWrapperProps {
  pageId: string;
}

export const AttributesWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin-bottom: ${(props: AttributesWrapperProps) => (props.pageId === 'Product' ? '24px' : '16px')};

  :last-child {
    margin-bottom: 0;
  }
`

const P = styled.p`
  padding: 0;
  margin: 0;
  color: ${colors.text};
  text-transform: uppercase;
`

interface AttributeNameProps {
  cartId?: string;
}

export const AttributeName = styled(P)`
  font-size: ${(props: AttributeNameProps) => (props.cartId === 'CartOverlay' ? '14px' : '18px')};
  line-height: ${(props: AttributeNameProps) => (props.cartId === 'CartOverlay' ? '16px' : '18px')};
  font-weight: ${(props: AttributeNameProps) => (props.cartId === 'CartOverlay' ? 400 : 700)};
  font-family: ${(props: AttributeNameProps) => (props.cartId === 'CartOverlay' ? fonts.Raleway : fonts.RobotoCondensed)};
  margin-bottom: 8px;
`

interface AttributeItemsWrapperProps {
  type: string;
  pageId: string;
  cartId?: string;
}

export const AttributeItemsWrapper = styled.div`
  display: grid;
  grid-gap: ${(props: AttributeItemsWrapperProps) => (props.type === 'swatch' ? '8px' : '12px')};
  grid-template-columns: ${(props: AttributeItemsWrapperProps) => (props.cartId === 'CartOverlay' ? 'repeat(3, 1fr)' : 'repeat(6, 1fr)')};
`

interface SwatchAttributeItemWrapperProps {
  selectedItemId: string | void;
  itemId: string;
  cartId?: string;
}

export const SwatchAttributeItemWrapper = styled.div`
  width: ${(props: SwatchAttributeItemWrapperProps) => (props.cartId === 'CartOverlay' ? '20px' : '34px')};
  height: ${(props: SwatchAttributeItemWrapperProps) => (props.cartId === 'CartOverlay' ? '20px' : '34px')};
  border: ${(props: SwatchAttributeItemWrapperProps) => (props.itemId === props.selectedItemId
    ? `1px solid ${colors.primary} `
    : '1px solid transparent')};
`

interface SwatchAttributeItemProps {
  value: string;
  pageId: string;
  cartId?: string;
}

export const SwatchAttributeItem = styled.div`
  width: ${(props: SwatchAttributeItemProps) => (props.cartId === 'CartOverlay' ? '18px' : '32px')};
  height: ${(props: SwatchAttributeItemProps) => (props.cartId === 'CartOverlay' ? '18px' : '32px')};
  background: ${(props: SwatchAttributeItemProps) => props.value};
  cursor: pointer;
  border: 1px solid
    ${(props: SwatchAttributeItemProps) => (props.value === '#FFFFFF' ? colors.text : colors.white)};
`

interface TextAttributeItemProps {
  selectedItemId: string | void;
  itemId: string;
  cartId?: string;
}

export const TextAttributeItem = styled(P)`
  border: 1px solid ${colors.text};
  background: ${(props: TextAttributeItemProps) => (props.itemId === props.selectedItemId ? colors.text : colors.white)};
  color: ${(props: TextAttributeItemProps) => (props.itemId === props.selectedItemId ? colors.white : colors.text)};
  width: ${(props: TextAttributeItemProps) => (props.cartId === 'CartOverlay' ? '24px' : '63px')};
  height: ${(props: TextAttributeItemProps) => (props.cartId === 'CartOverlay' ? '24px' : '45px')};
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;

  font-size: ${(props: TextAttributeItemProps) => (props.cartId === 'CartOverlay' ? '11px' : '16px')};
  line-height: ${(props: TextAttributeItemProps) => (props.cartId === 'CartOverlay' ? '160%' : '18px')};
  font-family: ${fonts.SourceSansPro};
  font-weight: 400;
  cursor: pointer;
`
