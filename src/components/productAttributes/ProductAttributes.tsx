import React, { Component } from 'react'
// Redux
import { connect } from 'react-redux'
import {
  setSelectedAttributesAC,
  resetSelectedAttributesAC,
} from '../../redux/reducers/productReducer'
// Styles
import {
  AttributesWrapper,
  AttributeName,
  AttributeItemsWrapper,
  SwatchAttributeItemWrapper,
  SwatchAttributeItem,
  TextAttributeItem,
} from './productAttributesStyle'

type Props = typeof ProductAttributes.defaultProps & {
  readonly pageId: string;
  productId?: string;
  readonly attributes: Attribute[];
  selectedAttributes: Record<string, string>;
  setSelectedAttributesAC: (attributeId: string, itemId: string) => object;
  resetSelectedAttributesAC: (selectedAttributes: object) => object;
  cartId?: string;
}

interface ItemOfAttribute {
  readonly displayValue: string;
  readonly id: string;
  readonly value: string;
}

interface Attribute {
  readonly id: string;
  readonly name: string;
  readonly type: string;
  readonly items: ItemOfAttribute[];
}

class ProductAttributes extends Component<Props> {
  // eslint-disable-next-line react/static-property-placement
  static defaultProps = { productId: '', cartId: '' }

  onSelectAttribute = async (attributeId: string, itemId: string) => {
    if (this.props.pageId === 'Product') {
      this.props.setSelectedAttributesAC(attributeId, itemId)
    }
  }

  componentWillUnmount() {
    if (this.props.pageId === 'Product') {
      this.props.resetSelectedAttributesAC({})
    }
  }

  render() {
    return (
      <>
        {this.props.attributes.map((attribute: Attribute) => (
          <AttributesWrapper pageId={this.props.pageId} key={attribute.id}>
            <AttributeName cartId={this.props.cartId}>
              {attribute.name}
              :
            </AttributeName>
            <AttributeItemsWrapper
              type={attribute.type}
              pageId={this.props.pageId}
              cartId={this.props.cartId}
            >
              {attribute.items.map((item: ItemOfAttribute) => {
                const selectedItemId = this.props.selectedAttributes[attribute.id]

                if (attribute.type === 'swatch') {
                  return (
                    <SwatchAttributeItemWrapper
                      selectedItemId={selectedItemId}
                      itemId={item.id}
                      cartId={this.props.cartId}
                      key={item.id}
                    >
                      <SwatchAttributeItem
                        onClick={() => this.onSelectAttribute(attribute.id, item.id)}
                        value={item.value}
                        pageId={this.props.pageId}
                        cartId={this.props.cartId}
                      />
                    </SwatchAttributeItemWrapper>
                  )
                } return (
                  <TextAttributeItem
                    onClick={() => this.onSelectAttribute(attribute.id, item.id)}
                    selectedItemId={selectedItemId}
                    itemId={item.id}
                    key={item.id}
                    cartId={this.props.cartId}
                  >
                    {item.value}
                  </TextAttributeItem>
                )
              })}
            </AttributeItemsWrapper>
          </AttributesWrapper>
        ))}
      </>
    )
  }
}

const mapStateToProps = () => ({})

export default connect(mapStateToProps, {
  setSelectedAttributesAC,
  resetSelectedAttributesAC,
})(ProductAttributes)
