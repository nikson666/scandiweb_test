import React, { Component } from 'react'
// Redux
import { connect } from 'react-redux'
import { setIndexOfImageAC } from '../../redux/reducers/productReducer'
import { RootState } from '../../redux/store'
// Styles
import {
  GalleryWrapper,
  Image,
  ImagesWrapper,
  GeneralyImage,
  GeneralyImageWrapper,
} from './productGalleryStyle'

interface Props {
  gallery: string[];
  indexOfImage: number;
  setIndexOfImageAC: (index: number) => object;
}

class ProductGallery extends Component<Props> {
  onSelectImage = (indexOfImage: number) => {
    this.props.setIndexOfImageAC(indexOfImage)
  }

  componentWillUnmount() {
    this.props.setIndexOfImageAC(0)
  }

  render() {
    return (
      <GalleryWrapper>
        <ImagesWrapper>
          {this.props.gallery.map((image, index) => (
            <Image
              onClick={() => this.onSelectImage(index)}
              key={image.toString()}
              src={image}
            />
          ))}
        </ImagesWrapper>
        <GeneralyImageWrapper>
          <GeneralyImage src={this.props.gallery[this.props.indexOfImage]} />
        </GeneralyImageWrapper>
      </GalleryWrapper>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  indexOfImage: state.product.indexOfImage,
})

export default connect(mapStateToProps, { setIndexOfImageAC })(ProductGallery)
