import styled from 'styled-components'
import { colors } from '../../styles/commonStyles'

export const GalleryWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
`

export const ImagesWrapper = styled.div`
  display: grid;
  grid-template: repeat(auto-fill, minmax(80px, auto));
  grid-gap: 40px;
  justify-items: center;

  margin-right: 35px;
  overflow: auto;
  overflow-x: hidden;
  max-height: 511px;
  padding-right: 5px;

  ::-webkit-scrollbar {
    width: 5px;
  }

  ::-webkit-scrollbar-thumb {
    background: ${colors.text};
    border-radius: 2.5px;
  }
`

export const GeneralyImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 610px;
  height: 511px;
`

export const GeneralyImage = styled.img`
  max-width: 100%;
  max-height: 100%;
`

export const Image = styled.img`
  max-width: 80px;
  cursor: pointer;
`
