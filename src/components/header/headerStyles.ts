import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { Arrow, colors, fonts } from '../../styles/commonStyles'

export const HeaderWrapper = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 80px;
`

export const NavList = styled.div`
  display: flex;
  align-items: flex-start;
  text-align: center;
  text-transform: uppercase;
  margin: 0;
  padding: 0;
  margin-top: 24px;
`

export const NavElem = styled(NavLink)`
  color: ${colors.text};
  border-bottom: 2px solid transparent;
  font-family: ${fonts.Raleway};
  font-weight: 600;
  font-size: 16px;
  text-decoration: none;
  padding: 4px 16px 32px;
  cursor: pointer;

  &.active {
    color: ${colors.primary};
    border-bottom: 2px solid ${colors.primary};
  }
`

export const Logo = styled.img`
  position: absolute;
  left: 50%;
  transform: translate(-50%, 0);
  width: 41px;
  height: 41px;
`

export const Actions = styled.div`
  display: flex;
  align-items: center;
  padding: 0 16px;
`

export const Label = styled.p`
  padding: 0;
  margin: 0;
  font-family: ${fonts.Raleway};
  font-weight: 500;
  font-size: 18px;
  line-height: 28.8px;
  margin-right: 10px;
`

export const Currencies = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  margin-right: 22px;
  cursor: pointer;
`

export const CurrenciesArrow = styled(Arrow)`
  transform: ${(props) => (props.theme.flag ? 'rotate(225deg)' : 'rotate(45deg)')};
`

export const Cart = styled.div`
  position: relative;
  padding: 1px;
  cursor: pointer;
`

export const CartImg = styled.img`
  position: relative;
  z-index: 100;
  width: 20px;
  height: 20px;
`

interface CounterCartProps {
  counter: number;
}

export const CounterCart = styled.p`
  padding: 0;
  margin: 0;
  display: ${(props: CounterCartProps) => (props.counter >= 1 ? 'flex' : 'none')};
  justify-content: center;
  align-items: center;
  background-color: ${colors.text};
  width: 20px;
  height: 20px;
  border-radius: 50%;
  position: absolute;
  top: -9.17px;
  right: -13px;

  color: ${colors.white};
  font-size: 14px;
  font-weight: 700;
  font-family: ${fonts.Roboto};
  line-height: 16px;
  text-transform: uppercase;
`
