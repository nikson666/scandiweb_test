import React, { Component } from 'react'
// Apollo
import { gql } from '@apollo/client'
import { Query } from '@apollo/react-components'
// Styles
import { connect } from 'react-redux'
import { Container, Wrapper } from '../../styles/commonStyles'
import {
  HeaderWrapper,
  Logo,
  Actions,
  Currencies,
  Label,
  CurrenciesArrow,
  Cart,
  CartImg,
  NavList,
  NavElem,
  CounterCart,
} from './headerStyles'
// Assets
import logo from '../../assets/images/brandIcon.png'
import emptyCart from '../../assets/images/emptyCart.png'
// Routes
import { routes } from '../../_routes'
// Redux
import { setActiveSelectorFlagAC } from '../../redux/reducers/currenciesReducer'
import { RootState } from '../../redux/store'
import {
  setIsCartOverlayAC,
  setProductsOfCartAC,
} from '../../redux/reducers/cartReducer'
// components
import CurrenciesSelector from './CurrenciesSelector/CurrenciesSelector'
// Utils
import { getProductsOfCartFromLocalStorage, getCounter } from '../../utils'

interface Props {
  readonly currencySymbol: string;
  readonly activeSelectorFlag: boolean;
  setActiveSelectorFlagAC: (flag: boolean) => object;
  setProductsOfCartAC: (productOfCart: ProductOfCart[]) => object;
  productsOfCart: ProductOfCart[];
  isCartOverlay: boolean;
  setIsCartOverlayAC: (isCartOverlay: boolean) => object;
  productId: string;
}

interface ProductOfCart {
  productId: string;
  selectedAttributes: Record<string, string>;
  uniqKey: string;
  counter: number;
  prices: ProductOfCartPrice[];
}

interface ProductOfCartPrice {
  readonly amount: number;
  readonly currency: {
    label: string;
    symbol: string;
  };
}

interface QueryDateCategoties {
  readonly name: string;
}

interface QueryDataCurrencies {
  readonly label: string;
  readonly symbol: string;
}

interface QueriesData {
  categories: QueryDateCategoties[];
  currencies: QueryDataCurrencies[];
}

const GET_CATEGORIES_AND_CURRENCIES = gql`
  query {
    categories {
      name
    }
    currencies {
      label
      symbol
    }
  }
`

class Header extends Component<Props> {
  componentDidMount() {
    getProductsOfCartFromLocalStorage()
      .then((res) => {
        localStorage.setItem('ScandiawebCart', JSON.stringify(res))
        return res
      })
      .then((res) => {
        if (res) {
          this.props.setProductsOfCartAC(res)
        }
      })
  }

  onCurrenciesSelectorHandler = (flag: boolean) => {
    this.props.setActiveSelectorFlagAC(flag)
  }

  onCartHandler = () => {
    this.props.setIsCartOverlayAC(!this.props.isCartOverlay)
  }

  render() {
    return (
      <Query query={GET_CATEGORIES_AND_CURRENCIES}>
        {({ data }: { data: QueriesData }) => {
          const counter = getCounter(this.props.productsOfCart)
          return (
            <Wrapper>
              {this.props.activeSelectorFlag ? (
                <CurrenciesSelector currencies={data ? data.currencies : []} />
              ) : null}
              <Container>
                <HeaderWrapper>
                  <NavList>
                    {data
                      ? data.categories.map((item) => (
                        <NavElem
                          to={`${routes.categoryRoute}=${item.name}`}
                          key={item.name}
                        >
                          {item.name}
                        </NavElem>
                      ))
                      : null}
                  </NavList>
                  <Logo src={logo} />
                  <Actions>
                    <Currencies
                      onClick={() => this.onCurrenciesSelectorHandler(
                        !this.props.activeSelectorFlag,
                      )}
                    >
                      <Label>{this.props.currencySymbol}</Label>
                      <CurrenciesArrow
                        theme={{ flag: this.props.activeSelectorFlag }}
                      />
                    </Currencies>
                    <Cart onClick={this.onCartHandler}>
                      <CartImg src={emptyCart} />
                      <CounterCart counter={counter}>{counter}</CounterCart>
                    </Cart>
                  </Actions>
                </HeaderWrapper>
              </Container>
            </Wrapper>
          )
        }}
      </Query>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  currencySymbol: state.currencies.currencySymbol,
  activeSelectorFlag: state.currencies.activeSelectorFlag,
  productsOfCart: state.cart.productsOfCart,
  isCartOverlay: state.cart.isCartOverlay,
  productId: state.product.productId,
})

export default connect(mapStateToProps, {
  setActiveSelectorFlagAC,
  setProductsOfCartAC,
  setIsCartOverlayAC,
})(Header)
