import React, { Component } from 'react'
// Styles
import { connect } from 'react-redux'
import {
  CurrencyWrapper,
  CurrencyText,
  BackgroundCurrency,
  CurrencyContainer,
} from './CurrenciesSelectorStyles'
// Redux
import {
  setCurrencySymbolAC,
  setActiveSelectorFlagAC,
} from '../../../redux/reducers/currenciesReducer'
import { RootState } from '../../../redux/store'

interface Currency {
  readonly label: string
  readonly symbol: string
}

interface Props {
  currencies: Currency[]
  setCurrencySymbolAC: (currencySymbol: string) => object
  activeSelectorFlag: boolean
  setActiveSelectorFlagAC: (flag: boolean) => object
}

class CurrenciesSelector extends Component<Props> {
  onSelectCurrency = async (currencySymbol: string) => {
    await this.props.setCurrencySymbolAC(currencySymbol)
    this.props.setActiveSelectorFlagAC(false)
  }

  closeCurrenciesSelector = (flag: boolean) => {
    this.props.setActiveSelectorFlagAC(!flag)
  }

  render() {
    return (
      <BackgroundCurrency
        onClick={() => this.closeCurrenciesSelector(this.props.activeSelectorFlag)}
      >
        <CurrencyContainer>
          <CurrencyWrapper onClick={(e) => e.stopPropagation()}>
            {this.props.currencies.map((currency) => (
              <CurrencyText
                onClick={() => this.onSelectCurrency(currency.symbol)}
                key={currency.label}
              >
                {`${currency.symbol} ${currency.label}`}
              </CurrencyText>
            ))}
          </CurrencyWrapper>
        </CurrencyContainer>
      </BackgroundCurrency>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  activeSelectorFlag: state.currencies.activeSelectorFlag,
})

export default connect(mapStateToProps, { setCurrencySymbolAC, setActiveSelectorFlagAC })(
  CurrenciesSelector,
)
