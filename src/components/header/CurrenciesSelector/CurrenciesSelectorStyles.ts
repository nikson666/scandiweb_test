import styled from 'styled-components'
import { colors, Container, fonts } from '../../../styles/commonStyles'
import { CartOverlayBackground } from '../../cartOverlay/CartOverlayStyles'

export const BackgroundCurrency = styled(CartOverlayBackground)`
  background: transparent;
`

export const CurrencyWrapper = styled.ul`
  position: absolute;
  z-index: 999;
  top: 35px;
  right: 12px;
  padding: 0;
  box-shadow: 0px 4px 35px rgba(168, 172, 176, 0.19);
`

export const CurrencyContainer = styled(Container)`
  position: relative;
`

export const CurrencyText = styled.li`
  position: relative;
  z-index: 999;
  padding: 10px 30px 10px 10px;
  margin: 0;
  font-size: 18px;
  font-family: ${fonts.Raleway};
  font-weight: 500;
  line-height: 28.8px;
  list-style: none;
  cursor: pointer;

  &:hover {
    background-color: ${colors.onHoverCurrency};
  }
`
