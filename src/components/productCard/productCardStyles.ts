import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { colors, fonts } from '../../styles/commonStyles'

export const CardWrapper = styled(NavLink)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;

  padding: 16px;
  width: 386px;
  text-decoration: none;
  cursor: pointer;
  transition: all 100ms ease-in;

  &:hover {
    box-shadow: 0px 4px 35px ${colors.hoverCard};
  }
`

export const Image = styled.img`
  max-width: 354px;
  max-height: 330px;
  margin-bottom: 24px;
`

export const BriefInfo = styled.div`
  width: 100%;
  display: flex ;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
`
export const Naming = styled.p`
  font-weight: 300;
  font-family: ${fonts.Raleway};
  font-size: 18px;
  line-height: 28.8px;
  color: ${colors.text};
  padding: 0;
  margin: 0;
`

export const ProductPrice = styled(Naming)`
  font-weight: 500;
`
