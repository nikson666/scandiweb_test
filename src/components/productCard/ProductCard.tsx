import React, { Component } from 'react'
// Redux
import { connect } from 'react-redux'
import { RootState } from '../../redux/store'
// Routes
import { routes } from '../../_routes'
// Styles
import {
  CardWrapper,
  Image,
  BriefInfo,
  Naming,
  ProductPrice,
} from './productCardStyles'
import { setItemToSessionStorage } from '../../utils'

interface Price {
  readonly amount: number;
  readonly currency: {
    label: string;
    symbol: string;
  };
}

interface Props {
  readonly brand: string;
  readonly name: string;
  readonly image: string;
  readonly price: Price;
  readonly id: string;
}

class ProductCard extends Component<Props> {
  onSelectProduct = (productId: string) => {
    setItemToSessionStorage('productId', productId)
  }

  render() {
    return (
      <CardWrapper
        onClick={() => this.onSelectProduct(this.props.id)}
        to={`${routes.productRoute}=${this.props.id}`}
      >
        <Image src={this.props.image} />
        <BriefInfo>
          <Naming>{`${this.props.brand} ${this.props.name}`}</Naming>
          <ProductPrice>
            {`${this.props.price.currency.symbol}${this.props.price.amount}`}
          </ProductPrice>
        </BriefInfo>
      </CardWrapper>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  productId: state.product.productId,
})

export default connect(mapStateToProps, {})(ProductCard)
