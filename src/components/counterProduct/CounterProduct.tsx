import React, { Component } from 'react'
// Refux
import { connect } from 'react-redux'
import { setChangeCounterAC } from '../../redux/reducers/cartReducer'
import { RootState } from '../../redux/store'
// Styles
import {
  Count,
  Decrement,
  Increment,
  CounterWrapper,
} from './counterProductStyle'

interface Price {
  readonly amount: number;
  readonly currency: {
    label: string;
    symbol: string;
  };
}

interface ProductOfCart {
  productId: string;
  selectedAttributes: Record<string, string>;
  uniqKey: string;
  counter: number;
  prices: Price[];
}

interface Props {
  counter: number;
  productId: string;
  uniqKey: string;
  productsOfCart: ProductOfCart[];
  setChangeCounterAC: (newProductsOfCart: ProductOfCart[]) => object;
  cartId: string;
}

class CounterProduct extends Component<Props> {
  onIncrement = async () => {
    try {
      const newProductsOfCart = await this.props.productsOfCart.map(
        (product) => {
          if (
            product.uniqKey === this.props.uniqKey
            && product.productId === this.props.productId
          ) {
            return {
              ...product,
              counter: this.props.counter + 1,
            }
          } return product
        },
      )
      this.props.setChangeCounterAC(newProductsOfCart)
    } catch (error) {
      console.log('ERROR', error)
    }
  }

  onDecrement = async () => {
    try {
      if (this.props.counter !== 1) {
        const newProductsOfCart = await this.props.productsOfCart.map(
          (product) => {
            if (
              product.uniqKey === this.props.uniqKey
              && product.productId === this.props.productId
            ) {
              return {
                ...product,
                counter: this.props.counter - 1,
              }
            } return product
          },
        )
        this.props.setChangeCounterAC(newProductsOfCart)
      }
    } catch (error) {
      console.log('ERROR', error)
    }
  }

  render() {
    return (
      <CounterWrapper cartId={this.props.cartId}>
        <Increment cartId={this.props.cartId} onClick={() => this.onIncrement()} />
        <Count cartId={this.props.cartId}>{this.props.counter}</Count>
        <Decrement cartId={this.props.cartId} onClick={() => this.onDecrement()} />
      </CounterWrapper>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  productsOfCart: state.cart.productsOfCart,
})

export default connect(mapStateToProps, {
  setChangeCounterAC,
})(CounterProduct)
