import styled from 'styled-components'
import { colors, fonts } from '../../styles/commonStyles'

interface Props {
  cartId: string;
}

export const Decrement = styled.button`
  padding: ${(props: Props) => (props.cartId === 'CartOverlay' ? '12px' : '23px')};
  border: 1px solid ${colors.text};
  position: relative;
  cursor: pointer;
  background-color: transparent;
  ::before {
    background-color: ${colors.text};
    position: absolute;
    content: "";
    width: ${(props: Props) => (props.cartId === 'CartOverlay' ? '8px' : '15px')};
    border: 1px solid ${colors.text};
    border-radius: 1px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  :active {
    background-color: ${colors.backgroundArrowCartGallery};
  }
`

export const Increment = styled(Decrement)`
  ::after {
    background-color: ${colors.text};
    position: absolute;
    content: "";
    width: ${(props: Props) => (props.cartId === 'CartOverlay' ? '8px' : '15px')};
    border: 1px solid ${colors.text};
    border-radius: 1px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) rotate(90deg);
  }
  `

export const Count = styled.p`
  padding: 0;
  margin: 0;
  font-size: ${(props: Props) => (props.cartId === 'CartOverlay' ? '16px' : '24px')};
  line-height: 160%;
  font-weight: 500;
  font-family: ${fonts.Raleway};
  color: ${colors.text};
`

export const CounterWrapper = styled.div`
  width: max-content;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin-right: ${(props: Props) => (props.cartId === 'CartOverlay' ? '5px' : '24px')};
`
