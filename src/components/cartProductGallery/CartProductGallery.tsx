import React, { Component } from 'react'
// Styles
import {
  GalleryWrapper,
  Image,
  ArrowsWrapper,
  WrapOfArrow,
  ArrowLeft,
  ArrowRight,
} from './cartProductGalleryStyle'

interface Props {
  images: string[]
  cartId: string
}

interface State {
  indexOfImage: number
}

class CartProductGallery extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      indexOfImage: 0,
    }
  }

  nextImage = () => {
    if (this.state.indexOfImage < this.props.images.length - 1) {
      this.setState((prevState) => ({
        ...prevState,
        indexOfImage: prevState.indexOfImage + 1,
      }))
    } else {
      this.setState((prevState) => ({
        ...prevState,
        indexOfImage: 0,
      }))
    }
  }

  prevImage = () => {
    if (this.state.indexOfImage === 0) {
      this.setState((prevState) => ({
        ...prevState,
        indexOfImage: this.props.images.length - 1,
      }))
    } else {
      this.setState((prevState) => ({
        ...prevState,
        indexOfImage: prevState.indexOfImage - 1,
      }))
    }
  }

  render() {
    return (
      <GalleryWrapper cartId={this.props.cartId}>
        <Image src={this.props.images[this.state.indexOfImage]} alt="image" />
        <ArrowsWrapper
          cartId={this.props.cartId}
          countOfImages={this.props.images.length}
        >
          <WrapOfArrow onClick={() => this.prevImage()}>
            <ArrowLeft />
          </WrapOfArrow>
          <WrapOfArrow onClick={() => this.nextImage()}>
            <ArrowRight />
          </WrapOfArrow>
        </ArrowsWrapper>
      </GalleryWrapper>
    )
  }
}

export default CartProductGallery
