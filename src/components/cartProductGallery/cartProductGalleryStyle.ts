import styled from 'styled-components'
import { Arrow, colors } from '../../styles/commonStyles'

interface GalleryWrapperProps {
  cartId: string
}

export const GalleryWrapper = styled.div`
  max-height: ${(props: GalleryWrapperProps) => (props.cartId === 'CartOverflay' ? '190px' : '288px')};
  max-width: ${(props: GalleryWrapperProps) => (props.cartId === 'CartOverflay' ? '121px' : '200px')};
  position: relative;
  align-items: center;
  display: flex;
`

interface ArrowWrapperProps {
  countOfImages: number
  cartId: string
}

export const ArrowsWrapper = styled.div`
  position: absolute;
  bottom: 16px;
  right: 16px;
  width: 56px;
  display: ${(props: ArrowWrapperProps) => {
    if (props.countOfImages === 1) return 'none'
    if (props.cartId === 'CartOverlay') return 'none'
    return 'flex'
  }};
  justify-content: space-between;
`

export const WrapOfArrow = styled.div`
  width: 24px;
  height: 24px;
  background-color: ${colors.backgroundArrowCartGallery};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`

const BaseArrow = styled(Arrow)`
  border-width: 0 1.5px 1.5px 0;
  padding: 1.5px;
  border-color: ${colors.white};
  border-radius: 1px;
`

export const ArrowRight = styled(BaseArrow)`
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);
`

export const ArrowLeft = styled(BaseArrow)`
  transform: rotate(135deg);
  -webkit-transform: rotate(135deg);
`

export const Image = styled.img`
  max-height: 100%;
  max-width: 100%;
`
