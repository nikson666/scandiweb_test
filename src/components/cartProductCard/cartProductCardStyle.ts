import styled from 'styled-components'
import { Brand, Name, Price } from '../../pages/product/productStyles'
import { colors } from '../../styles/commonStyles'

interface Props {
  cartId: string;
}

export const CartProductWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  width: 100%;
  padding: 24px 0;
  border-bottom: ${(props: Props) => (props.cartId === 'CartOverlay' ? '0px' : '1px')}
      solid ${colors.gary};
  :first-child {
    border-top: ${(props: Props) => (props.cartId === 'CartOverlay' ? '0px' : '1px')}
      solid ${colors.gary};
  }
  :last-child {
    margin-bottom: 32px;
  }
`
export const LeftSideWrapper = styled.div`
  width: max-content;
  max-width: 50%;
`

export const RightSideWrapper = styled(LeftSideWrapper)`
  display: flex;
`

export const ProductBrand = styled(Brand)`
  font-weight: ${(props: Props) => (props.cartId === 'CartOverlay' ? 300 : 600)};
  font-size: ${(props: Props) => (props.cartId === 'CartOverlay' ? '16px' : '30px')};
  line-height: ${(props: Props) => (props.cartId === 'CartOverlay' ? '160%' : '27px')};
  margin-bottom: ${(props: Props) => (props.cartId === 'CartOverlay' ? '5px' : '20px')};
`

export const ProductName = styled(Name)`
  font-weight: ${(props: Props) => (props.cartId === 'CartOverlay' ? 300 : 400)};
  font-size: ${(props: Props) => (props.cartId === 'CartOverlay' ? '16px' : '30px')};
  line-height: ${(props: Props) => (props.cartId === 'CartOverlay' ? '160%' : '27px')};
  margin-bottom: ${(props: Props) => (props.cartId === 'CartOverlay' ? '5px' : '20px')};
`

export const ProductPrice = styled(Price)`
  font-weight: ${(props: Props) => (props.cartId === 'CartOverlay' ? 500 : 700)};
  font-size: ${(props: Props) => (props.cartId === 'CartOverlay' ? '16px' : '24px')};
  line-height: ${(props: Props) => (props.cartId === 'CartOverlay' ? '160%' : '24px')};
`

export const RemoveBtn = styled.button`
  position: absolute;
  width: 15px;
  height: 15px;
  top: 5px;
  right: 5px;
  border: none;
  border-radius: 3px;
  background: transparent;
  cursor: pointer;

  :active {
    background-color: ${colors.text};
  }

  ::before {
    position: absolute;
    content: "";
    background-color: ${colors.text};
    width: 15px;
    border: 1px solid ${colors.text};
    border-radius: 1px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) rotate(-45deg);
  }
  ::after {
    position: absolute;
    content: "";
    background-color: ${colors.text};
    width: 15px;
    border: 1px solid ${colors.text};
    border-radius: 1px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) rotate(45deg);
  }
`
