import React, { Component } from 'react'
// Apollo
import { gql } from '@apollo/client'
import { Query } from '@apollo/react-components'
// Redux
import { connect } from 'react-redux'
import { RootState } from '../../redux/store'
import { setProductsOfCartAC } from '../../redux/reducers/cartReducer'
// Utils
import { getIndexOfPrice } from '../../utils'
// Components
import ProductAttributes from '../productAttributes/ProductAttributes'
import CartProductGallery from '../cartProductGallery/CartProductGallery'
// Styles
import {
  CartProductWrapper,
  ProductBrand,
  ProductName,
  ProductPrice,
  LeftSideWrapper,
  RightSideWrapper,
  RemoveBtn,
} from './cartProductCardStyle'
import CounterProduct from '../counterProduct/CounterProduct'

interface Props {
  productId: string;
  readonly currencySymbol: string;
  selectedAttributes: Record<string, string>;
  counter: number;
  uniqKey: string;
  productsOfCart: ProductOfCart[];
  setProductsOfCartAC: (productsOfCart: ProductOfCart[]) => object;
  cartId: string;
}

interface ProductOfCart {
  productId: string;
  selectedAttributes: Record<string, string>;
  uniqKey: string;
  counter: number;
  prices: QueryDataProductPrice[];
}

interface QueryDataProductAttributeItem {
  readonly displayValue: string;
  readonly id: string;
  readonly value: string;
}

interface QueryDataProductAttributes {
  readonly id: string;
  readonly name: string;
  readonly type: string;
  readonly items: QueryDataProductAttributeItem[];
}

interface QueryDataProductPrice {
  readonly amount: number;
  readonly currency: {
    label: string;
    symbol: string;
  };
}

interface QueryData {
  readonly product: {
    attributes: QueryDataProductAttributes[];
    brand: string;
    category: string;
    description: string;
    gallery: string[];
    id: string;
    inStock: boolean;
    name: string;
    prices: QueryDataProductPrice[];
  };
}

const GET_PRODUCT_CART = gql`
  query product($productId: String!) {
    product(id: $productId) {
      id
      inStock
      name
      brand
      gallery
      description
      category
      attributes {
        id
        name
        type
        items {
          displayValue
          value
          id
        }
      }
      prices {
        amount
        currency {
          label
          symbol
        }
      }
    }
  }
`

class CartProductCard extends Component<Props> {
  async onRemoveProductFromCart(uniqKey: string) {
    try {
      const productsOfCart = await [...this.props.productsOfCart]
      const indexOfRemoveProduct: number = await productsOfCart?.findIndex(
        (product) => product.uniqKey === uniqKey,
      )
      await productsOfCart.splice(indexOfRemoveProduct, 1)
      this.props.setProductsOfCartAC(productsOfCart)
    } catch (error) {
      console.log('ERROR', error)
    }
  }

  render() {
    return (
      <Query
        query={GET_PRODUCT_CART}
        variables={{ productId: this.props.productId }}
        fetchPolicy="no-cache"
      >
        {({ data, loading }: { data: QueryData; loading: boolean }) => {
          if (loading) <h1>Loading...</h1>

          if (data) {
            const indexOfPrice = getIndexOfPrice(
              data.product.prices,
              this.props.currencySymbol,
            )

            return (
              <CartProductWrapper cartId={this.props.cartId}>
                <LeftSideWrapper>
                  <ProductBrand cartId={this.props.cartId}>
                    {data.product.brand}
                  </ProductBrand>
                  <ProductName cartId={this.props.cartId}>
                    {data.product.name}
                  </ProductName>
                  <ProductPrice cartId={this.props.cartId}>
                    {data.product.prices[indexOfPrice].currency.symbol}
                    {data.product.prices[indexOfPrice].amount}
                  </ProductPrice>
                  <ProductAttributes
                    selectedAttributes={this.props.selectedAttributes}
                    attributes={data.product.attributes}
                    pageId="Cart"
                    cartId={this.props.cartId}
                    productId={data.product.id}
                  />
                </LeftSideWrapper>
                <RightSideWrapper>
                  <CounterProduct
                    productId={this.props.productId}
                    uniqKey={this.props.uniqKey}
                    counter={this.props.counter}
                    cartId={this.props.cartId}
                  />
                  <CartProductGallery
                    cartId={this.props.cartId}
                    images={data.product.gallery}
                  />
                </RightSideWrapper>
                <RemoveBtn
                  onClick={() => this.onRemoveProductFromCart(this.props.uniqKey)}
                />
              </CartProductWrapper>
            )
          } return null
        }}
      </Query>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  productsOfCart: state.cart.productsOfCart,
  currencySymbol: state.currencies.currencySymbol,
})

export default connect(mapStateToProps, { setProductsOfCartAC })(
  CartProductCard,
)
