import React, { Component } from 'react'
// Redux
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { RootState } from '../../redux/store'
import {
  setProductsOfCartAC,
  setIsCartOverlayAC,
} from '../../redux/reducers/cartReducer'
// Styles
import {
  CartOverlayBackground,
  CartOverlayWrapper,
  CartOverlayProducts,
  HeaderCartOverlay,
  HeaderTitle,
  HeaderInfo,
  ActionsWrapper,
  CheckOut,
  ViewBag,
  TotalWrap,
  TotalTitle,
  TotalPrice,
} from './CartOverlayStyles'
// Components
import CartProductCard from '../cartProductCard/CartProductCard'
// Utils
import {
  getCounter,
  getTotalPrice,
  setProductsOfCartOnLocalStorage,
} from '../../utils'
// Router
import { routes } from '../../_routes'

interface ProductOfCartPrice {
  readonly amount: number;
  readonly currency: {
    label: string;
    symbol: string;
  };
}

interface ProductOfCart {
  productId: string;
  selectedAttributes: Record<string, string>;
  uniqKey: string;
  counter: number;
  prices: ProductOfCartPrice[];
}

interface Props {
  productsOfCart: ProductOfCart[];
  currencySymbol: string;
  setProductsOfCartAC: (productsOfCart: ProductOfCart[]) => object;
  setIsCartOverlayAC: (isCartOverlay: boolean) => object;
  isCartOverlay: boolean;
}

class CartOverlay extends Component<Props> {
  componentWillUnmount() {
    setProductsOfCartOnLocalStorage(this.props.productsOfCart)
  }

  closeCartHandler = () => {
    try {
      this.props.setIsCartOverlayAC(false)
    } catch (error) {
      console.log('ERROR', error)
    }
  }

  checkOutBtnHandler = async () => {
    try {
      if (this.props.productsOfCart.length) {
        await this.props.setProductsOfCartAC([])
        this.closeCartHandler()
      }
    } catch (error) {
      console.log('ERROR', error)
    }
  }

  render() {
    return (
      <CartOverlayBackground onClick={this.closeCartHandler}>
        <CartOverlayWrapper onClick={(e) => e.stopPropagation()}>
          <HeaderCartOverlay>
            <HeaderTitle>My Bag</HeaderTitle>
            <HeaderInfo>
              {`, ${getCounter(
                this.props.productsOfCart,
              )} items`}
            </HeaderInfo>
          </HeaderCartOverlay>
          <CartOverlayProducts>
            {this.props.productsOfCart.length ? (
              this.props.productsOfCart.map((product) => (
                <CartProductCard
                  key={product.uniqKey}
                  productId={product.productId}
                  selectedAttributes={product.selectedAttributes}
                  counter={product.counter}
                  uniqKey={product.uniqKey}
                  cartId="CartOverlay"
                />
              ))
            ) : (
              <h2>Cart is empty</h2>
            )}
          </CartOverlayProducts>
          <TotalWrap>
            <TotalTitle>Total</TotalTitle>
            <TotalPrice>
              {`${this.props.currencySymbol} ${getTotalPrice(
                this.props.productsOfCart,
                this.props.currencySymbol,
              )}`}
            </TotalPrice>
          </TotalWrap>
          <ActionsWrapper>
            <NavLink onClick={this.closeCartHandler} to={routes.cartRoute}>
              <ViewBag>view bag</ViewBag>
            </NavLink>
            <CheckOut onClick={this.checkOutBtnHandler}>check out</CheckOut>
          </ActionsWrapper>
        </CartOverlayWrapper>
      </CartOverlayBackground>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  productsOfCart: state.cart.productsOfCart,
  currencySymbol: state.currencies.currencySymbol,
  isCartOverlay: state.cart.isCartOverlay,
})

export default connect(mapStateToProps, {
  setProductsOfCartAC,
  setIsCartOverlayAC,
})(CartOverlay)
