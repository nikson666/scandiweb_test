import styled from 'styled-components'
import { colors, fonts } from '../../styles/commonStyles'

export const CartOverlayBackground = styled.div`
  position: absolute;
  z-index: 100;

  width: 100vw;
  height: 100%;
  min-height: 100vh;

  background-color: ${colors.backgroundActiveOverlayCart};
  display: flex;
`

export const CartOverlayWrapper = styled.div`
  position: absolute;
  top: 78px;
  right: 72px;
  background-color: ${colors.white};
  width: 305px;
  max-height: 583px;
  padding: 32px 16px;

  display: flex;
  justify-content: center;
  flex-direction: column;
`

export const CartOverlayProducts = styled.div`
  position: relative;
  overflow: auto;

  display: flex;
  justify-content: flex-start;
  flex-direction: column;

  ::-webkit-scrollbar {
    width: 5px;
  }

  ::-webkit-scrollbar-thumb {
    background: ${colors.text};
    border-radius: 2.5px;
  }
`

export const HeaderCartOverlay = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 100%;
  position: relative;
  padding-bottom: 5px;
`

export const HeaderTitle = styled.div`
  font-family: ${fonts.Raleway};
  font-size: 16px;
  font-weight: 700;
  line-height: 160%;
`

export const HeaderInfo = styled(HeaderTitle)`
  font-weight: 500;
`

export const ActionsWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-top: 5px;
`

export const ViewBag = styled.button`
  width: 140px;
  height: 43px;

  border: 1px solid ${colors.text};
  color: ${colors.text};
  background-color: transparent;

  font-family: ${fonts.Raleway};
  font-size: 14px;
  font-weight: 600;
  line-height: 120%;

  text-transform: uppercase;
  cursor: pointer;

  :active {
    background-color: ${colors.text};
    color: ${colors.white};
  }
`

export const CheckOut = styled(ViewBag)`
  border: 1px solid ${colors.primary};
  background-color: ${colors.primary};
  color: ${colors.white};
`

export const TotalWrap = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const TotalTitle = styled.p`
  padding: 0;
  margin: 0;
  font-family: ${fonts.Roboto};
  font-size: 16px;
  font-weight: 500;
  line-height: 18px;
`

export const TotalPrice = styled.p`
  padding: 0;
  margin: 0;
  font-family: ${fonts.Raleway};
  font-weight: 700;
  line-height: 160%;
  font-size: 16px;
`
