import React from 'react'
import { connect } from 'react-redux'
import { Outlet } from 'react-router-dom'
import { RootState } from '../redux/store'
import { Container, Wrapper } from '../styles/commonStyles'
import CartOverlay from './cartOverlay/CartOverlay'
import Header from './header/Header'

interface Props {
  isCartOverlay: boolean
}

class RouterLayout extends React.PureComponent<Props> {
  render() {
    const { isCartOverlay } = this.props
    return (
      <Wrapper>
        {isCartOverlay ? <CartOverlay /> : null}
        <Header />
        <Container>
          <Outlet />
        </Container>
      </Wrapper>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  isCartOverlay: state.cart.isCartOverlay,
})

export default connect(mapStateToProps)(RouterLayout)
