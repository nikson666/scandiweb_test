import React from 'react'
// Router
import { Route, Routes } from 'react-router-dom'
import { routes } from './_routes'
// Components
import Cart from './pages/cart/Cart'
import Category from './pages/category/Category'
import Product from './pages/product/Product'
import RouterLayout from './components/RouterLayout'
import withRouter from './HOC/withRouter'

const CategoryWithRouter = withRouter(Category)
const ProductWithRouter = withRouter(Product)

class App extends React.PureComponent {
  render(): React.ReactNode {
    return (
      <Routes>
        <Route path="/" element={<RouterLayout />}>
          <Route index element={<h2>Choose Category</h2>} />
          <Route
            path={`${routes.categoryRoute}=:selectedCategory`}
            element={<CategoryWithRouter />}
          />
          <Route
            path={`:category=:selectedCategory/${routes.productRoute}=:productId`}
            element={<ProductWithRouter />}
          />
          <Route path={routes.cartRoute} element={<Cart />} />
        </Route>
      </Routes>
    )
  }
}

export default App
