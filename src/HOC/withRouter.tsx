import React from 'react'
import { useLocation } from 'react-router-dom'

function withRouter(WrappedComponent: any) {
  return function fn(props: any) {
    const location = useLocation()
    return <WrappedComponent {...props} location={location} />
  }
}

export default withRouter
