import styled from 'styled-components'
import { colors, fonts } from '../../styles/commonStyles'

export const CartWrapper = styled.div`
  width: 100%;
  padding-bottom: 274px;
`

export const CartProductsWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const CartTitle = styled.p`
  padding: 0;
  margin: 0;
  font-size: 32px;
  font-weight: 700;
  font-family: ${fonts.Raleway};
  color: ${colors.text};
  line-height: 40px;
  margin-bottom: 55px;
`

export const OrderWrapper = styled.div`
  display: flex;
  flex-direction: flex;
  flex-direction: column;
  justify-content: space-between;
  max-width: 280px;
`

export const OrderInfoWrapper = styled(OrderWrapper)`
  flex-direction: row;
  margin-bottom: 16px;
  justify-content: flex-start;
`

export const TitleWrap = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding-right: 5px;
`

export const InfoWrap = styled(TitleWrap)``

export const Title = styled.p`
  padding: 0;
  margin: 0;
  font-size: 24px;
  line-height: 28px;
  font-weight: 400;
  font-family: ${fonts.Raleway};
`

export const Info = styled(Title)`
  font-weight: 700;
`

export const OrderBtn = styled.button`
  padding: 13px 115.5px;
  text-transform: uppercase;
  background-color: ${colors.primary};
  border: none;
  font-size: 14px;
  line-height: 120%;
  font-weight: 600;
  font-family: ${fonts.Raleway};
  color: ${colors.white};
  cursor: pointer;

  :active {
    background-color: ${colors.text};
  }
`
