/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react'
// Redux
import { connect } from 'react-redux'
import { RootState } from '../../redux/store'
import { setProductsOfCartAC } from '../../redux/reducers/cartReducer'
// Components
import CartProductCard from '../../components/cartProductCard/CartProductCard'
// Styles
import {
  CartWrapper,
  CartProductsWrapper,
  CartTitle,
  OrderWrapper,
  OrderInfoWrapper,
  TitleWrap,
  Title,
  InfoWrap,
  Info,
  OrderBtn,
} from './cartStyle'
// Utils
import {
  getCounter,
  getIndexOfPrice,
  setProductsOfCartOnLocalStorage,
} from '../../utils'

interface ProductOfCartPrice {
  readonly amount: number
  readonly currency: {
    label: string
    symbol: string
  }
}

interface ProductOfCart {
  productId: string
  selectedAttributes: Record<string, string>
  uniqKey: string
  counter: number
  prices: ProductOfCartPrice[]
}

interface Props {
  productsOfCart: ProductOfCart[]
  currencySymbol: string
  setProductsOfCartAC: (productsOfCart: ProductOfCart[]) => object
}

class Cart extends Component<Props> {
  componentWillUnmount() {
    setProductsOfCartOnLocalStorage(this.props.productsOfCart)
  }

  orderBtnHandler() {
    try {
      if (this.props.productsOfCart.length) {
        this.props.setProductsOfCartAC([])
      }
    } catch (error) {
      console.log('ERROR', error)
    }
  }

  getOrderInfo() {
    try {
      if (this.props.productsOfCart.length) {
        const indexOfPrice = getIndexOfPrice(
          this.props.productsOfCart[0]?.prices,
          this.props.currencySymbol,
        )

        const arrayOfTotalAmountOf: number[] = this.props.productsOfCart.map(
          (item) => item.prices[indexOfPrice].amount * item.counter,
        )

        const totalAmountOf = arrayOfTotalAmountOf?.reduce(
          (accumulator, value) => accumulator + value,
        )

        const quantity: number = getCounter(this.props.productsOfCart)

        const tax: number = (totalAmountOf / 100) * 21

        return [
          `${this.props.currencySymbol}${tax.toFixed(2)}`,
          quantity,
          `${this.props.currencySymbol}${totalAmountOf.toFixed(2)}`,
        ]
      }
      return [0, 0, 0]
    } catch (error) {
      console.log('ERROR', error)
    }
  }

  render() {
    return (
      <CartWrapper>
        <CartTitle>Cart</CartTitle>
        <CartProductsWrapper>
          {this.props.productsOfCart.length ? (
            this.props.productsOfCart.map((product) => (
              <CartProductCard
                key={product.uniqKey}
                productId={product.productId}
                selectedAttributes={product.selectedAttributes}
                counter={product.counter}
                uniqKey={product.uniqKey}
                cartId="CartPage"
              />
            ))
          ) : (
            <h1>Cart is empty</h1>
          )}
        </CartProductsWrapper>
        <OrderWrapper>
          <OrderInfoWrapper>
            <TitleWrap>
              {['Tax 21%:', 'Quantity:', 'Total:'].map(
                (title: string, index: number) => (
                  <Title key={index}>{title}</Title>
                ),
              )}
            </TitleWrap>
            <InfoWrap>
              {this.getOrderInfo()?.map(
                (info: number | string, index: number) => (
                  <Info key={index}>{info}</Info>
                ),
              )}
            </InfoWrap>
          </OrderInfoWrapper>
          <OrderBtn onClick={() => this.orderBtnHandler()}>OREDR</OrderBtn>
        </OrderWrapper>
      </CartWrapper>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  productsOfCart: state.cart.productsOfCart,
  currencySymbol: state.currencies.currencySymbol,
})

export default connect(mapStateToProps, { setProductsOfCartAC })(Cart)
