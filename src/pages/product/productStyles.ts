import styled from 'styled-components'
import { colors, fonts } from '../../styles/commonStyles'

export const ProductWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
`

export const ProductInfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 292px;
  margin-left: 100px;
`

export const Brand = styled.p`
  color: ${colors.text};
  font-family: ${fonts.Raleway};
  font-weight: 600;
  font-size: 30px;
  line-height: 27px;
  margin: 0 0 16px 0;
  padding: 0;
`

export const Name = styled(Brand)`
  font-weight: 400;
  margin-bottom: 43px;
`

export const PriceTitle = styled.p`
  padding: 0;
  margin: 0;

  font-family: ${fonts.RobotoCondensed};
  font-weight: 700;
  font-size: 18px;
  line-height: 18px;
  color: ${colors.text};
  margin-bottom: 10px;
  text-transform: uppercase;
`

export const Price = styled.p`
  font-family: ${fonts.Raleway};
  font-weight: 700;
  font-size: 24px;
  line-height: 24px;
  padding: 0;
  margin: 0 0 20px 0;
`

export const AddToCartBtn = styled.button`
    font-size: 16px;
    font-weight: 600;
    font-family: ${fonts.Raleway};
    line-height: 120%;
    text-transform: uppercase;
    color: ${colors.white};
    background: ${colors.primary};
    
    padding: 16px 93.5px;
    margin-bottom: 40px;
    text-decoration: none;
    text-align: center;
    cursor: pointer;

    border: none;

    :active {
      background-color: ${colors.text};
    }
`

export const Description = styled.p`
    font-size: 16px;
    font-weight: 400;
    font-family: ${fonts.Roboto};
    line-height: 160%;
    color: ${colors.text};
    max-height: 200px;
    overflow: auto;

    ::-webkit-scrollbar {
        width: 5px;
    }

    ::-webkit-scrollbar-thumb {
        background: ${colors.text} ;
        border-radius: 2.5px;
    }
`
