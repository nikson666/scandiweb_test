import React, { Component } from 'react'
// Apollo
import { gql } from '@apollo/client'
import { Query } from '@apollo/react-components'
// Redux
import { connect } from 'react-redux'
import { RootState } from '../../redux/store'
import { setProductsOfCartAC } from '../../redux/reducers/cartReducer'
import { setProducIdAC } from '../../redux/reducers/productReducer'
// Components
import ProductGallery from '../../components/productGallery/ProductGallery'
import ProductAttributes from '../../components/productAttributes/ProductAttributes'
// Styles
import {
  ProductWrapper,
  ProductInfoWrapper,
  Brand,
  Name,
  PriceTitle,
  Price,
  AddToCartBtn,
  Description,
} from './productStyles'
// Utils
import { getIndexOfPrice, setProductsOfCartOnLocalStorage } from '../../utils'

interface Props {
  readonly productId: string;
  readonly currencySymbol: string;
  selectedAttributes: Record<string, string>;
  productsOfCart: ProductOfCart[];
  setProductsOfCartAC: (newProductsOfCart: ProductOfCart[]) => object;
  setProducIdAC: (productId: string) => object;
  location: Location;
}

interface Location {
  pathname: string;
  search: string;
  hash: string;
  state: null;
  key: string;
}

interface ProductOfCart {
  productId: string;
  selectedAttributes: Record<string, string>;
  uniqKey: string;
  counter: number;
  prices: QueryDataProductPrice[];
}

interface QueryDataProductAttributeItem {
  readonly displayValue: string;
  readonly id: string;
  readonly value: string;
}

interface QueryDataProductAttributes {
  readonly id: string;
  readonly name: string;
  readonly type: string;
  readonly items: QueryDataProductAttributeItem[];
}

interface QueryDataProductPrice {
  readonly amount: number;
  readonly currency: {
    label: string;
    symbol: string;
  };
}

interface QueryData {
  readonly product: {
    attributes: QueryDataProductAttributes[];
    brand: string;
    category: string;
    description: string;
    gallery: string[];
    id: string;
    inStock: boolean;
    name: string;
    prices: QueryDataProductPrice[];
  };
}

const GET_PRODUCT = gql`
  query product($productId: String!) {
    product(id: $productId) {
      id
      inStock
      name
      brand
      gallery
      description
      category
      attributes {
        id
        name
        type
        items {
          displayValue
          value
          id
        }
      }
      prices {
        amount
        currency {
          label
          symbol
        }
      }
    }
  }
`

class Product extends Component<Props> {
  componentDidMount() {
    const arrayOfPathPieces = this.props.location.pathname.split('=')
    const productId = arrayOfPathPieces[arrayOfPathPieces.length - 1]
    this.props.setProducIdAC(productId)
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.location !== this.props.location) {
      const arrayOfPathPieces = this.props.location.pathname.split('=')
      const productId = arrayOfPathPieces[arrayOfPathPieces.length - 1]
      this.props.setProducIdAC(productId)
    }
  }

  addToCartHandler = async (
    selectedAttributes: Record<string, string>,
    attributesOfProduct: QueryDataProductAttributes[],
    productId: string,
    productPrice: QueryDataProductPrice[],
  ) => {
    try {
      // checking that user selected all attributes (validation)
      let isValidated = false
      const arrayOfSelectedAttributesId = Object.keys(selectedAttributes)
      const productAttributes: Array<string> = []
      for (let i = 0; i < attributesOfProduct.length; i++) {
        const attributeId = attributesOfProduct[i].id
        productAttributes.push(attributeId)
      }
      if (productAttributes.length === arrayOfSelectedAttributesId.length) isValidated = true

      // adding new product to cart
      if (isValidated) {
        const oldProductsOfCart = this.props.productsOfCart
        let newProductsOfCart: ProductOfCart[] = []
        let isAdded = false
        const product = {
          productId,
          selectedAttributes,
          uniqKey: Math.random() + productId,
          counter: 1,
          prices: productPrice,
        }
        newProductsOfCart = await oldProductsOfCart.map((item) => {
          if (
            this.props.productId === item.productId
            && this.props.selectedAttributes === item.selectedAttributes
          ) {
            isAdded = true
            return {
              ...item,
              counter: item.counter + 1,
            }
          } return item
        })

        if (!isAdded) newProductsOfCart.push(product)

        await setProductsOfCartOnLocalStorage(newProductsOfCart)
        this.props.setProductsOfCartAC(newProductsOfCart)
      } else alert('Select all attributes of product for add product to cart!')
    } catch (error) {
      console.log('ERROR:', error)
    }
  }

  render() {
    return (
      <Query
        query={GET_PRODUCT}
        variables={{ productId: this.props.productId }}
        fetchPolicy="no-cache"
      >
        {({ data, loading }: { data: QueryData; loading: boolean }) => {
          if (loading) <h1>Loading...</h1>
          if (data) {
            const indexOfPrices = getIndexOfPrice(
              data.product.prices,
              this.props.currencySymbol,
            )

            return (
              <ProductWrapper>
                <ProductGallery gallery={data.product.gallery} />
                <ProductInfoWrapper>
                  <Brand>{data.product.brand}</Brand>
                  <Name>{data.product.name}</Name>
                  <ProductAttributes
                    attributes={data.product.attributes}
                    pageId="Product"
                    selectedAttributes={this.props.selectedAttributes}
                  />
                  <PriceTitle>price:</PriceTitle>
                  <Price>
                    {data.product.prices[indexOfPrices].currency.symbol}
                    {data.product.prices[indexOfPrices].amount}
                  </Price>
                  <AddToCartBtn
                    onClick={() => this.addToCartHandler(
                      this.props.selectedAttributes,
                      data.product.attributes,
                      data.product.id,
                      data.product.prices,
                    )}
                  >
                    add to cart
                  </AddToCartBtn>
                  <Description
                    dangerouslySetInnerHTML={{
                      __html: data.product.description,
                    }}
                  />
                </ProductInfoWrapper>
              </ProductWrapper>
            )
          } return null
        }}
      </Query>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  productId: state.product.productId,
  currencySymbol: state.currencies.currencySymbol,
  selectedAttributes: state.product.selectedAttributes,
  productsOfCart: state.cart.productsOfCart,
})

export default connect(mapStateToProps, {
  setProductsOfCartAC,
  setProducIdAC,
})(Product)
