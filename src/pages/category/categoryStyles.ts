import styled from 'styled-components'
import { colors, fonts } from '../../styles/commonStyles'

export const CategoryName = styled.p`
  padding: 0;
  margin: 0;
  font-family: ${fonts.Raleway};
  font-weight: 400;
  font-size: 42px;
  line-height: 67.2px;
  color: ${colors.text};
  font-style: normal;
  text-transform: capitalize;
  margin-bottom: 100px;
`

export const ContentWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(386px, auto));
  width: 100%;
  grid-gap: 40px;
  justify-items: center;
  padding-bottom: 190px;
`
