import React, { Component } from 'react'
// Styles
import { gql } from '@apollo/client'
import { Query } from '@apollo/react-components'
import { connect } from 'react-redux'
import { Wrapper } from '../../styles/commonStyles'
import { CategoryName, ContentWrapper } from './categoryStyles'
// Apollo
// Redux
import { RootState } from '../../redux/store'
import { setActiveCategoryAC } from '../../redux/reducers/categoriesReducer'
// Components
import ProductCard from '../../components/productCard/ProductCard'
// Utils
import { getIndexOfPrice } from '../../utils'

interface Props {
  readonly activeCategory: string;
  readonly currencySymbol: string;
  setActiveCategoryAC: (category: string) => object;
  readonly location: Location;
}

interface Location {
  pathname: string;
  search: string;
  hash: string;
  state: null;
  key: string;
}

interface QueryDataCategoryProductPrice {
  readonly amount: number;
  readonly currency: {
    label: string;
    symbol: string;
  };
}

interface QueryDataCategoryProduct {
  readonly gallery: string[];
  readonly name: string;
  readonly id: string;
  readonly prices: QueryDataCategoryProductPrice[];
  readonly brand: string;
}

interface QueryData {
  readonly category: {
    name: string;
    products: QueryDataCategoryProduct[];
  };
}

const GET_CATEGORY = gql`
  query category($category: String!) {
    category(input: { title: $category }) {
      name
      products {
        name
        brand
        prices {
          currency {
            label
            symbol
          }
          amount
        }
        gallery
        id
      }
    }
  }
`

class Category extends Component<Props> {
  componentDidMount() {
    const arrayOfPathPieces = this.props.location.pathname.split('=')
    const activeCategory = arrayOfPathPieces[arrayOfPathPieces.length - 1]
    this.props.setActiveCategoryAC(activeCategory)
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.location !== this.props.location) {
      const arrayOfPathPieces = this.props.location.pathname.split('=')
      const activeCategory = arrayOfPathPieces[arrayOfPathPieces.length - 1]
      this.props.setActiveCategoryAC(activeCategory)
    }
  }

  render() {
    return (
      <Query
        query={GET_CATEGORY}
        variables={{
          category: this.props.activeCategory,
        }}
      >
        {({ data }: { data: QueryData }) => {
          if (data) {
            const indexOfPrice = getIndexOfPrice(
              data.category.products[0].prices,
              this.props.currencySymbol,
            )

            return (
              <Wrapper>
                <CategoryName>{this.props.activeCategory}</CategoryName>
                <ContentWrapper>
                  {data.category.products.map((product) => (
                    <ProductCard
                      key={product.id}
                      id={product.id}
                      brand={product.brand}
                      name={product.name}
                      image={product.gallery[0]}
                      price={product.prices[indexOfPrice]}
                    />
                  ))}
                </ContentWrapper>
              </Wrapper>
            )
          } return null
        }}
      </Query>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  activeCategory: state.categories.activeCategory,
  currencySymbol: state.currencies.currencySymbol,
})

export default connect(mapStateToProps, {
  setActiveCategoryAC,
})(Category)
